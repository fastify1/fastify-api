import Fastify from 'fastify';
import fastifySwagger from '@fastify/swagger';
import fastifySwaggerUi from '@fastify/swagger-ui';
import itemRoutes from './routes/items.js';

const fastify = Fastify({ logger: true });

fastify.register(fastifySwagger, {});
fastify.register(fastifySwaggerUi, {
  exposeRoute: true,
  routePrefix: '/docs',
  swagger: {
    info: {
      title: 'fastify-api'
    }
  }
});

fastify.register(itemRoutes);

const PORT = 3000;

const start = async () => {
  try {
    await fastify.listen({ port: PORT })
  } catch (error) {
    fastify.log.error(error);
    process.exit()
  }
};

start();