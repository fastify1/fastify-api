## Name
Fastify API

## Description
This is a test project to get to know Fastify framework. It builds REST API with validation schemas and Swagger documentation.


## Installation
Run the following commands:

```
npm install
npm run dev
```

## Usage
You can use **test.http** file to test the app with **REST Client** extension.
Or go to your browser and open [http://localhost:3000](http://127.0.0.1:3000), [http://localhost:3000/items](http://127.0.0.1:3000/items) 

## Documentation
For Swagger documentation visit [http://localhost:3000/docs](http://127.0.0.1:3000/docs) 
