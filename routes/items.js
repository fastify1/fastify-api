import items from "../Items.js";
import { getItem, getItems, addItem, deleteItem, updateItem } from "../controllers/items.js";

//Item schema
const Item = {
  type: 'object',
  properties: {
    id: { type: 'string' },
    name: { type: 'string' }
  }
};

//Options for get all items
const getItemsOpts = {
  schema: {
    response: {   //use the response key to speed up JSON serialization
      //this also helps leakage of potentially sensitive data, since Fasify will serialize only the data present in the response schema. 
      200: {
        type: 'array',
        items: { //Item
          type: 'object',
          properties: {
            // id: { type: 'integer' }, // will not show the id in the output
            name: { type: 'string' }
          }
        }
      }
    },
    // handler: getItems
  }
};

//Options for get an item
const getItemOpts = {
  schema: {
    response: {
      200: Item
    }
  },
  handler: getItem
};

//Options for post an item
const postItemOpts = {
  schema: {
    body: {
      type: 'object',
      required: ['name'],
      properties: {
        name: { type: 'string' }
      }
    },
    response: {
      201: Item
    }
  },
  handler: addItem
};

//Options for delete an item
const deleteItemOpts = {
  schema: {
    response: {
      200: {
        type: 'object',
        properties: {
          message: { type: 'string' }
        }
      }
    }
  },
  handler: deleteItem
};

//Options for update an item
const updateItemOpts = {
  schema: {
    body: {
      type: 'object',
      required: ['newName'],
      properties: {
        newName: { type: 'string' }
      }
    },
    response: {
      200: Item
    }
  },
  handler: updateItem
};

function itemRoutes(fastify, options, done) {
  //Greet user
  fastify.get('/', async function handler(request, reply) {
    return { message: 'Hello World!' }
  });

  //Get all items
  //          endPoint,   options,       handler
  fastify.get('/items', getItemsOpts, (req, reply) => {
    reply.send(items);
  })

  //Get single item
  //              endPoint,   options,  handler function is in the options object
  fastify.get('/items/:id', getItemOpts);

  //Add item
  fastify.post('/items', postItemOpts);

  //Delete item
  fastify.delete('/items/:id', deleteItemOpts);

  //Update item
  fastify.put('/items/:id', updateItemOpts);

  done()
}

export default itemRoutes;