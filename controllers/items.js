import items from '../Items.js';
import { v4 as uuidv4 } from 'uuid';

export const getItems = (req, reply) => {
  reply.send(items);
  //return reply;   //also possible
  //return reply.send(items) or await reply.send(items) required inside async handler or we can create race conditions
};

export function getItem (req, reply) {

  const { id } = req.params;
  const item = items.find((item) => item.id === id);

  reply.send(item || { message: 'No Such Item' });
};

export const addItem = (req, reply) => {
  const { name } = req.body;
  const item = {
    id: uuidv4(),
    name,
  };

  items.push(item); // this data is not persistent

  reply.code(201).send(item);
};

export const deleteItem = (req, reply) => {
  const { id } = req.params;

  const i = items.findIndex(item => item.id === id);
  items.splice(i, 1);
  reply.send({ message: `Item with id ${id} has been removed!` });
};

export const updateItem = (req, reply) => {
  const { newName } = req.body;
  const { id } = req.params;

  const item = items.find(item => item.id === id);
  item.name = newName;

  reply.send(item);
}